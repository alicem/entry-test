#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define ENTRYTEST_TYPE_WINDOW (entrytest_window_get_type())

G_DECLARE_FINAL_TYPE (EntrytestWindow, entrytest_window, ENTRYTEST, WINDOW, GtkApplicationWindow)

G_END_DECLS
